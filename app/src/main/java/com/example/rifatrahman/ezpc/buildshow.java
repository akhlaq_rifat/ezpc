package com.example.rifatrahman.ezpc;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.content.Intent;
import android.widget.Button;
import android.widget.TextView;

import static com.example.rifatrahman.ezpc.homepage.least;
import static com.example.rifatrahman.ezpc.homepage.type;

public class buildshow extends Activity {
    TextView textView;
    TextView textView2;
    Button button2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buildshow);
        textView2= (TextView) findViewById(R.id.textView2);
        if(type.equals("Gaming PC"))
            textView2.setText("Minimum budget required for\nGaming PC:"+least);
        else
            textView2.setText("Minimum budget required for\nProfessional PC:"+least);
        button2 = (Button) findViewById(R.id.button2);
        button2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent( buildshow.this,homepage.class);
                startActivity(intent);
            }
        });
    }
}
