package com.example.rifatrahman.ezpc;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.rifatrahman.ezpc.Database.DBhelper;

import java.util.ArrayList;
import java.util.List;

import static com.example.rifatrahman.ezpc.homepage.bad;
import static com.example.rifatrahman.ezpc.homepage.ca1;
import static com.example.rifatrahman.ezpc.homepage.ca2;
import static com.example.rifatrahman.ezpc.homepage.gp1;
import static com.example.rifatrahman.ezpc.homepage.gp2;
import static com.example.rifatrahman.ezpc.homepage.hd1;
import static com.example.rifatrahman.ezpc.homepage.hd2;
import static com.example.rifatrahman.ezpc.homepage.mo1;
import static com.example.rifatrahman.ezpc.homepage.mo2;
import static com.example.rifatrahman.ezpc.homepage.pr1;
import static com.example.rifatrahman.ezpc.homepage.pr2;
import static com.example.rifatrahman.ezpc.homepage.pw1;
import static com.example.rifatrahman.ezpc.homepage.pw2;
import static com.example.rifatrahman.ezpc.homepage.ra1;
import static com.example.rifatrahman.ezpc.homepage.ra2;


import static com.example.rifatrahman.ezpc.homepage.total;
import static com.example.rifatrahman.ezpc.homepage.txtstr1;
import static com.example.rifatrahman.ezpc.homepage.txtstr2;
import static com.example.rifatrahman.ezpc.homepage.txtstr3;
import static com.example.rifatrahman.ezpc.homepage.txtstr4;
import static com.example.rifatrahman.ezpc.homepage.txtstr5;
import static com.example.rifatrahman.ezpc.homepage.txtstr6;
import static com.example.rifatrahman.ezpc.homepage.txtstr7;
import static com.example.rifatrahman.ezpc.homepage.txtstr8;
import static com.example.rifatrahman.ezpc.homepage.txtstr9;
import static com.example.rifatrahman.ezpc.homepage.txtstr10;
import static com.example.rifatrahman.ezpc.homepage.txtstr11;
import static com.example.rifatrahman.ezpc.homepage.txtstr12;
import static com.example.rifatrahman.ezpc.homepage.txtstr13;
import static com.example.rifatrahman.ezpc.homepage.txtstr14;
import static com.example.rifatrahman.ezpc.homepage.txtstr15;
import static com.example.rifatrahman.ezpc.homepage.txtstr16;
import static com.example.rifatrahman.ezpc.homepage.txtstr17;
import static com.example.rifatrahman.ezpc.homepage.txtstr18;
import static com.example.rifatrahman.ezpc.homepage.txtstr19;
import static com.example.rifatrahman.ezpc.homepage.txtstr20;
import static com.example.rifatrahman.ezpc.homepage.txtstr21;
import static com.example.rifatrahman.ezpc.homepage.txtstr22;

public class showbuild extends Activity {

    Button retry,change;

    Spinner s1,s2,s3,s4,s5,s6,s7;

    TextView text,text8;

    int best=total;




    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_showbuild);

        text = (TextView) findViewById(R.id.t1);
        text.setText(txtstr1);
        text = (TextView) findViewById(R.id.t2);
        text.setText(txtstr2);
        if(bad==1)
            text.setVisibility(View.GONE);
        text = (TextView) findViewById(R.id.t3);
        text.setText(txtstr3);
        text = (TextView) findViewById(R.id.t4);
        text.setText(txtstr4);
        text = (TextView) findViewById(R.id.t5);
        text.setText(txtstr5);
        text = (TextView) findViewById(R.id.t6);
        text.setText(txtstr6);
        text = (TextView) findViewById(R.id.t7);
        text.setText(txtstr7);
        text8 = (TextView) findViewById(R.id.t8);
        text8.setText(txtstr8);

        @SuppressLint("ResourceType") Spinner s=(Spinner) findViewById(R.layout.spinner_item_color);


//spinner value set

        s1 = (Spinner) findViewById(R.id.spin1);
        List<String> list = new ArrayList<String>();
        list.add(" Near Choices ");
        list.add(txtstr19);
        list.add(txtstr20);
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,R.layout.spinner_item_color, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        s1.setAdapter(dataAdapter);


        s2 = (Spinner) findViewById(R.id.spin2);
        list = new ArrayList<String>();
        list.add(" Near Choices ");
        list.add(txtstr21);
        list.add(txtstr22);
        dataAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item_color, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        s2.setAdapter(dataAdapter);
        if(bad==1)
            s2.setVisibility(View.GONE);

        s3 = (Spinner) findViewById(R.id.spin3);
        list = new ArrayList<String>();
        list.add(" Near Choices ");
        list.add(txtstr9);
        list.add(txtstr10);
        dataAdapter = new ArrayAdapter<String>(this,R.layout.spinner_item_color, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        s3.setAdapter(dataAdapter);


        s4 = (Spinner) findViewById(R.id.spin4);
        list = new ArrayList<String>();
        list.add(" Near Choices ");
        list.add(txtstr11);
        list.add(txtstr12);
        dataAdapter = new ArrayAdapter<String>(this,R.layout.spinner_item_color, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        s4.setAdapter(dataAdapter);


        s5 = (Spinner) findViewById(R.id.spin5);
        list = new ArrayList<String>();
        list.add(" Near Choices ");
        list.add(txtstr13);
        list.add(txtstr14);
        dataAdapter = new ArrayAdapter<String>(this,R.layout.spinner_item_color, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        s5.setAdapter(dataAdapter);


        s6 = (Spinner) findViewById(R.id.spin6);
        list = new ArrayList<String>();
        list.add(" Near Choices ");
        list.add(txtstr15);
        list.add(txtstr16);
        dataAdapter = new ArrayAdapter<String>(this,R.layout.spinner_item_color, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        s6.setAdapter(dataAdapter);


        s7 = (Spinner) findViewById(R.id.spin7);
        list = new ArrayList<String>();
        list.add(" Near Choices ");
        list.add(txtstr17);
        list.add(txtstr18);
        dataAdapter = new ArrayAdapter<String>(this,R.layout.spinner_item_color, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        s7.setAdapter(dataAdapter);


//onclicklisterner_creation

        retry = (Button) findViewById(R.id.back);
        retry.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent( showbuild.this,homepage.class);
                startActivity(intent);
            }
        });

        change = (Button) findViewById(R.id.apply);
        change.setOnClickListener (new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//selected choice in spinner

                String newlyselected= String.valueOf(s1.getSelectedItem());
                total=best;
                if(newlyselected==txtstr19)
                {
                    total=total+pr1;
                    txtstr8="Total Price= "+String.valueOf(total)+" BDT";
                }
                else if(newlyselected==txtstr20)
                {
                    total=total+pr2;
                    txtstr8="Total Price= "+String.valueOf(total)+" BDT";
                }
                else
                {
                    txtstr8="Total Price= "+String.valueOf(total)+" BDT";
                }

                if(bad==0) {
                    newlyselected = String.valueOf(s2.getSelectedItem());
                    if (newlyselected == txtstr21) {
                        total = total + gp1;
                        txtstr8 = "Total Price= " + String.valueOf(total) + " BDT";
                    } else if (newlyselected == txtstr22) {
                        total = total + gp2;
                        txtstr8 = "Total Price= " + String.valueOf(total) + " BDT";
                    } else {
                        txtstr8 = "Total Price= " + String.valueOf(total) + " BDT";
                    }
                }

                newlyselected= String.valueOf(s3.getSelectedItem());
                if(newlyselected==txtstr9)
                {
                    total=total+mo1;
                    txtstr8="Total Price= "+String.valueOf(total)+" BDT";
                }
                else if(newlyselected==txtstr10)
                {
                    total=total+mo2;
                    txtstr8="Total Price= "+String.valueOf(total)+" BDT";
                }
                else
                {
                    txtstr8="Total Price= "+String.valueOf(total)+" BDT";
                }

                newlyselected= String.valueOf(s4.getSelectedItem());
                if(newlyselected==txtstr11)
                {
                    total=total+ra1;
                    txtstr8="Total Price= "+String.valueOf(total)+" BDT";
                }
                else if(newlyselected==txtstr12)
                {
                    total=total+ra2;
                    txtstr8="Total Price= "+String.valueOf(total)+"/=BDT";
                }
                else
                {
                    txtstr8="Total Price= "+String.valueOf(total)+" BDT";
                }

                newlyselected= String.valueOf(s5.getSelectedItem());
                if(newlyselected==txtstr13)
                {
                    total=total+pw1;
                    txtstr8="Total Price= "+String.valueOf(total)+" BDT";
                }
                else if(newlyselected==txtstr14)
                {
                    total=total+pw2;
                    txtstr8="Total Price= "+String.valueOf(total)+" BDT";
                }
                else
                {
                    txtstr8="Total Price= "+String.valueOf(total)+" BDT";
                }


                newlyselected= String.valueOf(s6.getSelectedItem());
                if(newlyselected==txtstr15)
                {
                    total=total+ca1;
                    txtstr8="Total Price= "+String.valueOf(total)+" BDT";
                }
                else if(newlyselected==txtstr16)
                {
                    total=total+ca2;
                    txtstr8="Total Price= "+String.valueOf(total)+" BDT";
                }
                else
                {
                    txtstr8="Total Price= "+String.valueOf(total)+" BDT";
                }


                newlyselected= String.valueOf(s7.getSelectedItem());
                if(newlyselected==txtstr17)
                {
                    total=total+hd1;
                    txtstr8="Total Price= "+String.valueOf(total)+" BDT";
                }
                else if(newlyselected==txtstr18)
                {
                    total=total+hd2;
                    txtstr8="Total Price= "+String.valueOf(total)+" BDT";
                }
                else
                {
                    txtstr8="Total Price= "+String.valueOf(total)+" BDT";
                }

                int extra=best-total;
                String last="";
                if(extra<0)
                {
                    extra=extra*-1;
                    last="["+String.valueOf(best)+"+"+String.valueOf(extra)+"]";
                }
                else if(extra==0)
                {
                    last="";
                }
                else
                    last="["+String.valueOf(best)+"-"+String.valueOf(extra)+"]";


                txtstr8="Total Price= "+String.valueOf(total)+" BDT\n"+last;
                text8.setText(txtstr8);

            }
        });

    }
}
