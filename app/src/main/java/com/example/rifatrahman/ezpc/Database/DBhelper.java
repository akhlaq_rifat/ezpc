package com.example.rifatrahman.ezpc.Database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static com.example.rifatrahman.ezpc.Model.DbModel.DBname;
import static com.example.rifatrahman.ezpc.Model.DbModel.DBversion;
import static com.example.rifatrahman.ezpc.homepage.total;
import static com.example.rifatrahman.ezpc.homepage.budget;
import static com.example.rifatrahman.ezpc.homepage.least;
import static com.example.rifatrahman.ezpc.homepage.type;
import static com.example.rifatrahman.ezpc.homepage.x;
import static com.example.rifatrahman.ezpc.homepage.bad;

import static com.example.rifatrahman.ezpc.homepage.txtstr1;
import static com.example.rifatrahman.ezpc.homepage.txtstr2;
import static com.example.rifatrahman.ezpc.homepage.txtstr3;
import static com.example.rifatrahman.ezpc.homepage.txtstr4;
import static com.example.rifatrahman.ezpc.homepage.txtstr5;
import static com.example.rifatrahman.ezpc.homepage.txtstr6;
import static com.example.rifatrahman.ezpc.homepage.txtstr7;
import static com.example.rifatrahman.ezpc.homepage.txtstr8;
import static com.example.rifatrahman.ezpc.homepage.txtstr9;
import static com.example.rifatrahman.ezpc.homepage.txtstr10;
import static com.example.rifatrahman.ezpc.homepage.txtstr11;
import static com.example.rifatrahman.ezpc.homepage.txtstr12;
import static com.example.rifatrahman.ezpc.homepage.txtstr13;
import static com.example.rifatrahman.ezpc.homepage.txtstr14;
import static com.example.rifatrahman.ezpc.homepage.txtstr15;
import static com.example.rifatrahman.ezpc.homepage.txtstr16;
import static com.example.rifatrahman.ezpc.homepage.txtstr17;
import static com.example.rifatrahman.ezpc.homepage.txtstr18;
import static com.example.rifatrahman.ezpc.homepage.txtstr19;
import static com.example.rifatrahman.ezpc.homepage.txtstr20;
import static com.example.rifatrahman.ezpc.homepage.txtstr21;
import static com.example.rifatrahman.ezpc.homepage.txtstr22;

import static com.example.rifatrahman.ezpc.homepage.ca1;
import static com.example.rifatrahman.ezpc.homepage.ca2;
import static com.example.rifatrahman.ezpc.homepage.gp1;
import static com.example.rifatrahman.ezpc.homepage.gp2;
import static com.example.rifatrahman.ezpc.homepage.hd1;
import static com.example.rifatrahman.ezpc.homepage.hd2;
import static com.example.rifatrahman.ezpc.homepage.mo1;
import static com.example.rifatrahman.ezpc.homepage.mo2;
import static com.example.rifatrahman.ezpc.homepage.pr1;
import static com.example.rifatrahman.ezpc.homepage.pr2;
import static com.example.rifatrahman.ezpc.homepage.pw1;
import static com.example.rifatrahman.ezpc.homepage.pw2;
import static com.example.rifatrahman.ezpc.homepage.ra1;
import static com.example.rifatrahman.ezpc.homepage.ra2;

/**
 * Created by rifatrahman on 11/16/2017.
 */

public class DBhelper extends SQLiteOpenHelper {

    public static final String create_processor_table="create table processor(processor_name varchar2(30),core_count int,clock_speed float,gen varchar2(20), cache float, socket varchar2(20),price int);";
    public static final String create_gpu_table="create table gpu(gpu_name varchar2(100),memory int,memory_type varchar2(20),clock_speed float,memory_clock float,price int);";
    public static final String create_motherboard_table="create table motherboard(motherboard_name varchar2(100),socket varchar2(20),memory_type varchar2(20),chipset varchar2(30),price int);";
    public static final String create_ram_table="create table ram(ram_name varchar2(50),type varchar2(10),memory int,bus int,price int);";
    public static final String create_powersupply_table="create table powersupply(powersupply_name varchar2(50),spec varchar2(100),watt int,price int);";
    public static final String create_casing_table="create table casing(casing_name varchar2(70),price int);";
    public static final String create_harddisk_table="create table harddisk(hard_name varchar2(100),price int);";


    public DBhelper(Context context) {
        super(context, DBname , null , DBversion);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(create_processor_table);
        db.execSQL("insert into processor values('Intel Pentium G3260',2,3.30,'4th',3,'unknown',5000);");
        db.execSQL("insert into processor values('Intel PDC G4560',2,3.50,'7th',3,'LGA1151',4900);");
        db.execSQL("insert into processor values('Intel core-i3 7100',2,3.90,'7th',3,'LGA1151',9100);");
        db.execSQL("insert into processor values('Intel core-i5 7400',4,3.50,'7th',6,'LGA1151',14600);");
        db.execSQL("insert into processor values('Intel core-i5 7500',4,3.80,'7th',6,'LGA1151',16100);");
        db.execSQL("insert into processor values('Intel core-i5 7600',4,4.10,'7th',6,'LGA1151',18900);");
        db.execSQL("insert into processor values('Intel core-i7 7700',4,4.20,'7th',8,'LGA1151',25000);");
        db.execSQL("insert into processor values('Intel core-i7 7700k',4,4.50,'7th',8,'LGA1151',28300);");
        db.execSQL("insert into processor values('AMD Ryzen 5 1400',4,3.4,'zen',8,'AM4',15700);");
        db.execSQL("insert into processor values('AMD Ryzen 5 1500X',4,3.7,'zen',16,'AM4',17600);");
        db.execSQL("insert into processor values('AMD RYZEN 5 1600',6,3.6,'zen',16,'AM4',18500);");
        db.execSQL("insert into processor values('AMD RYZEN 7 1700',8,3.7,'zen',16,'AM4',26400);");
        db.execSQL("insert into processor values('AMD RYZEN 7 1700X',8,3.8,'zen',16,'AM4',32400);");
        db.execSQL("insert into processor values('AMD RYZEN 7 1800X',8,4.0,'zen',16,'AM4',41200);");

        db.execSQL(create_gpu_table);
        db.execSQL("insert into gpu values('ZOTAC GeForce GT 1030',2,'GDDR5',1468,6,7300);");
        db.execSQL("insert into gpu values('ZOTAC GeForce GTX 1050',2,'GDDR5',1455,7,13500);");
        db.execSQL("insert into gpu values('Gigabyte Radeon RX 560 Gaming OC',4,'GDDR5',1300,7,14500);");
        db.execSQL("insert into gpu values('Gigabyte GeForce GTX 1050 Ti OC',4,'GDDR5',1455,7,14500);");
        db.execSQL("insert into gpu values('MSI GEFORCE GTX 1050 TI OC',4,'GDDR5',1455,7,15700);");
        db.execSQL("insert into gpu values('ZOTAC GeForce GTX 1050Ti',4,'GDDR5',1417,7,15800);");
        db.execSQL("insert into gpu values('Gigabyte GeForce GTX-1050Ti Gaming',4,'GDDR5',1506,7,16000);");
        db.execSQL("insert into gpu values('ZOTAC GeForce GTX-1060 AMP Edition',3,'GDDR5',1797,8,24500);");
        db.execSQL("insert into gpu values('Gigabyte Radeon RX-580 AORUS Xtreme',8,'GDDR5',1439,8,26700);");
        db.execSQL("insert into gpu values('ZOTAC GeForce GTX-1060',6,'GDDR5',1708,8,29700);");
        db.execSQL("insert into gpu values('Gigabyte AORUS GeForce GTX 1060',6,'GDDR5',1873,9,33700);");
        db.execSQL("insert into gpu values('Gigabyte AMD Radeon RX-550 GamingOC',2,'GDDR5',1219,7,9300);");
        db.execSQL("insert into gpu values('Asus RX550-4G',4,'GDDR5',1183,7,10500);");
        db.execSQL("insert into gpu values('ZOTAC GeForce GTX 1060',6,'GDDR5',1771,8,32200);");
        db.execSQL("insert into gpu values('ZOTAC GeForce GTX 1070',8,'GDDR5',1835,8,47500);");
        db.execSQL("insert into gpu values('ZOTAC GeForce GTX 1080Ti',11,'GDDR5X',1721,11.2,77000);");

        db.execSQL(create_motherboard_table);
        db.execSQL("insert into motherboard values('MSI H110M PRO-VD PLUS','LGA1151','DDR4','Intel H110',5400);");
        db.execSQL("insert into motherboard values('MSI B150M Pro-VD','LGA1151','DDR4','Intel B150',6000);");
        db.execSQL("insert into motherboard values('MSI BAZOOKA DB250M BADR4 7th Gen.','LGA1151','DDR4','Intel B150',7500);");
        db.execSQL("insert into motherboard values('MSI B350M GAMING PRO','AM4','DDR4','AMD B350 Chipset',8700);");
        db.execSQL("insert into motherboard values('Asus ROG STRIX B250H GAMING','LGA1151','DDR4',' Intel B250',10100);");
        db.execSQL("insert into motherboard values('Gigabyte GA-H270-HD3','LGA1151','DDR4',' Intel H270 Express',10700);");
        db.execSQL("insert into motherboard values('MSI B250 GAMING PRO CARBON','LGA1151','DDR4',' Intel B250',11100);");
        db.execSQL("insert into motherboard values('MSI B350 TOMAHAWK','LGA1151','DDR4','AMD B350',11700);");
        db.execSQL("insert into motherboard values('Asus PRIME Z270M-PLUS','LGA1151','DDR4','Intel Z270',12700);");
        db.execSQL("insert into motherboard values('MSI Z170A SLI PLUS','LGA1151','DDR4',' Intel Z170',13800);");
        db.execSQL("insert into motherboard values('Asus ROG STRIX Z270H GAMING','LGA1151','DDR4',' Intel Z270',14800);");
        db.execSQL("insert into motherboard values('MSI Z170A Krait Gaming','LGA1151','DDR4',' Intel Z170 Express',15600);");
        db.execSQL("insert into motherboard values('Gigabyte GA-Z270X-Gaming K5','LGA1151','DDR4',' Intel Z270 Express',16700);");
        db.execSQL("insert into motherboard values('MSI Z270 GAMING PRO CARBON','LGA1151','DDR4',' Intel Z270',17700);");
        db.execSQL("insert into motherboard values('Gigabyte GA-Z270X-Gaming 5','LGA1151','DDR4','Intel Z270 Express',18200);");
        db.execSQL("insert into motherboard values('Asus Z170-PRO','LGA1151','DDR4','Intel Z170',18800);");
        db.execSQL("insert into motherboard values('MSI Z270 GAMING M5','LGA1151','DDR4','Intel Z270',19300);");
        db.execSQL("insert into motherboard values('Gigabyte GA-Z270X-Gaming 7','LGA1151','DDR4','Intel Z270 Express',21900);");
        db.execSQL("insert into motherboard values('MSI X99S Mpower','LGA1151','DDR4','Intel X99',23700);");
        db.execSQL("insert into motherboard values('MSI X99A GODLIKE Gaming','LGA1151','DDR4','Intel X99 Express',47300);");
        db.execSQL("insert into motherboard values('Gigabyte X299 AORUS Gaming 3','LGA1151','DDR4',' Intel X299 Express',27000);");
        db.execSQL("insert into motherboard values('Asus Maximus VIII Hero Alpha','LGA1151','DDR4','Intel Z170',28100);");
        db.execSQL("insert into motherboard values('Asus MAXIMUS VIII FORMULA','LGA1151','DDR4',' Intel Z170',34800);");
        db.execSQL("insert into motherboard values('MSI Z170A GAMING M9 ACK','LGA1151','DDR4','Intel Z170 Express',37800);");
        db.execSQL("insert into motherboard values('Asus Maximus VIII Extreme','LGA1151','DDR4','Intel Z170',42300);");
        db.execSQL("insert into motherboard values('Gigabyte GA-Z270X-Gaming 9','LGA1151','DDR4','Intel Z270 Express',44700);");

        db.execSQL(create_ram_table);
        db.execSQL("insert into ram values('Team VULCAN OC Gaming Memories','DDR4',8,3000,5300);");
        db.execSQL("insert into ram values('Elite PLUS U-Dimm Series Standard Memory','DDR4',8,2400,6100);");
        db.execSQL("insert into ram values('TEAM 14 Vulcan RED U DIMM-D4','DDR4',8,2400,6000);");
        db.execSQL("insert into ram values('Elite U-Dim Series Standard Memory','DDR4',4,2400,3500);");
        db.execSQL("insert into ram values('Team VULCAN OC Gaming Memories','DDR4',16,3000,9800);");

        db.execSQL(create_powersupply_table);
        db.execSQL("insert into powersupply values('Thermaltake Toughpower','Modular_ATX2.3_EPS2.92_A-PFC13.5cm_EU80_PLUS_Gold',1000,17500);");
        db.execSQL("insert into powersupply values('Thermaltake Toughpower Grand','Fully Modular ATX2.4_A-PFC14cm RGB Fan EU80Plus Gold',850,12400);");
        db.execSQL("insert into powersupply values('Thermaltake Toughpower Grand','Fully Modular ATX2.4_A-PFC14cmRGB Fan EU80Plus Gold',750,11200);");
        db.execSQL("insert into powersupply values('Cooler Master MasterWatt Lite','ATX12V_V2.31',700,7100);");
        db.execSQL("insert into powersupply values('Thermaltake Toughpower Grand','Fully Modular ATX2.4_APFC14cm RGB Fan EU80Plus Gold',650,10100);");
        db.execSQL("insert into powersupply values('Thermaltake SPS-630MPCBEU Smart SE','Modular ATX12V_V2.31',630,6100);");
        db.execSQL("insert into powersupply values('Cooler Master MasterWatt Lite','ATX12V_V2.31',600,5500);");
        db.execSQL("insert into powersupply values('Cooler Master Vanguard','80 Plus Gold Full Modular ATX12V_V2.31',550,8800);");
        db.execSQL("insert into powersupply values('Thermaltake SMART SE','Modular_87%_efficiency',530,5000);");
        db.execSQL("insert into powersupply values('Corsair CX500 V2','80plus Certified ATX12V_v2.3',500,5100);");
        db.execSQL("insert into powersupply values('Thermaltake Toughpower SFX','Fully Modular SFX APFC8cm EU80PLUS Gold',450,7500);");
        db.execSQL("insert into powersupply values('Thermaltake W0423RE Litepower','Black Non Modular AT12V_V2.2',450,3100);");
        db.execSQL("insert into powersupply values('Thermaltake TR2 S','Non Modular ATX2.3_A-PFC12cm EU80 PLUS White',350,3300);");
        db.execSQL("insert into powersupply values('Cooler Master Elite Power','ATX12V_V2.3',350,2500);");

        db.execSQL(create_casing_table);
        db.execSQL("insert into casing values('Golden Field A2 with 450W PSU',2000);");
        db.execSQL("insert into casing values('Armaggeddon Nanotron T1X',2900);");
        db.execSQL("insert into casing values('Thermaltake Versa N21',5000);");
        db.execSQL("insert into casing values('Thermaltake Versa N21 Snow/White',5200);");
        db.execSQL("insert into casing values('Thermaltake Versa C22 RGB',6100);");
        db.execSQL("insert into casing values('Thermaltake Versa C22 RGB Snow White',6400);");
        db.execSQL("insert into casing values('Cooler Master MasterBox Lite 5',6000);");
        db.execSQL("insert into casing values('NZXT S340 Razer Edition',10200);");
        db.execSQL("insert into casing values('NZXT Phantom Glossy Black',10900);");
        db.execSQL("insert into casing values('NZXT MANTA MATTE BLACK',12800);");

        db.execSQL(create_harddisk_table);
        db.execSQL("insert into harddisk values('transcend 500GB',2500);");
        db.execSQL("insert into harddisk values('transcend 1TB',4500);");
        db.execSQL("insert into harddisk values('transcend 2TB',9000);");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    int gplow=0,molow=0,ramlow=0,powlow=0,caslow=0,hddlow=0,now=0,i5_price=0;


    public void least_price()
    {

        SQLiteDatabase sq = this.getReadableDatabase();
        least=0;
        now=0;
        Cursor c=sq.rawQuery("select * from processor order by price ASC;", null);
        c.moveToFirst();
        if(!c.isAfterLast()) {
            now=c.getInt(c.getColumnIndex("price"));
            least+=now;
        }

        c=sq.rawQuery("select * from gpu order by price ASC;", null);
        c.moveToFirst();
        if (!c.isAfterLast()) {
            now=c.getInt(c.getColumnIndex("price"));
            gplow=now;
            if(type.equals("Gaming PC"))
                least+=now;
        }
        c=sq.rawQuery("select * from motherboard order by price ASC;", null);
        c.moveToFirst();
        if (!c.isAfterLast()) {
            now=c.getInt(c.getColumnIndex("price"));
            least+=now;
            molow=now;
        }
        c=sq.rawQuery("select * from ram order by price ASC;", null);
        c.moveToFirst();
        if (!c.isAfterLast()) {
            now=c.getInt(c.getColumnIndex("price"));
            least+=now;
            ramlow=now;
        }
        c=sq.rawQuery("select * from powersupply order by price ASC;", null);
        c.moveToFirst();
        if (!c.isAfterLast()) {
            now=c.getInt(c.getColumnIndex("price"));
            least+=now;
            powlow=now;
        }
        c=sq.rawQuery("select * from casing order by price ASC;", null);
        c.moveToFirst();
        if (!c.isAfterLast()) {
            now=c.getInt(c.getColumnIndex("price"));
            least+=now;
            caslow=now;
        }
        c=sq.rawQuery("select * from harddisk order by price ASC;", null);
        c.moveToFirst();
        if (!c.isAfterLast()) {
            now=c.getInt(c.getColumnIndex("price"));
            least+=now;
            hddlow=now;
        }

        c=sq.rawQuery("select * from processor where processor_name like '%core-i5%' order by price ASC;",null);
        c.moveToFirst();
        if (!c.isAfterLast()) {
            i5_price=c.getInt(c.getColumnIndex("price"));
        }


        sq.close();
    }




    public boolean checkvalidity(){
        if(budget>=least)
            return true;
        else
            return false;
    }





    public void getdata() {
        SQLiteDatabase sq = this.getReadableDatabase();
        total=0;


//motherboard
        x = (budget * 12) / 100;
        if(x<molow)
            x=molow;
        String newprice = String.valueOf(x);
        Cursor motherboard= sq.rawQuery("select * from motherboard where price<=" + newprice + " order by price DESC", null);
        motherboard.moveToFirst();
        if (!motherboard.isAfterLast()) {
            txtstr3 = "MOTHERBOARD\n" + motherboard.getString(motherboard.getColumnIndex("motherboard_name")) + "\n" + "Socket: " + motherboard.getString(motherboard.getColumnIndex("socket")) + "\n"+ "memory: DDR4" + "\n" + "chipset: " + motherboard.getString(motherboard.getColumnIndex("chipset")) + "\n" + "price: " + motherboard.getInt(motherboard.getColumnIndex("price"));
            now = motherboard.getInt(motherboard.getColumnIndex("price"));
            total =now;

            newprice=String.valueOf(now);
            motherboard=sq.rawQuery("select * from motherboard where price <" + newprice + " order by price DESC",null);
            motherboard.moveToFirst();
            if (!motherboard.isAfterLast()) {
                txtstr9 =motherboard.getString(motherboard.getColumnIndex("motherboard_name"));
                x = motherboard.getInt(motherboard.getColumnIndex("price"))-now;
                mo1 = x;
                txtstr9 += "[" + String.valueOf(x) + "BDT]";


                motherboard=sq.rawQuery("select * from motherboard where price >" + newprice + " order by price ASC",null);
                motherboard.moveToFirst();
                if (!motherboard.isAfterLast()) {
                    txtstr10 = motherboard.getString(motherboard.getColumnIndex("motherboard_name"));
                    x=motherboard.getInt(motherboard.getColumnIndex("price"))-now;
                    mo1=x;
                    txtstr10 += "[+" + String.valueOf(x) + "BDT]";

                }
            }

        }



//ram
        x = (budget * 12) / 100;
        if(x<ramlow)
            x=ramlow;
        newprice = String.valueOf(x);
        Cursor ram= sq.rawQuery("select * from ram where price<=" + newprice + " order by price DESC", null);
        ram.moveToFirst();
        if(!ram.isAfterLast()) {
            txtstr4 = "RAM\n" + ram.getString(ram.getColumnIndex("ram_name")) + "\n" + "type: DDR4" + "\n" + "memory: " + ram.getInt(ram.getColumnIndex("memory")) + "GB\n" + "BUS: " + ram.getInt(ram.getColumnIndex("bus")) + "\n" + "price: " + ram.getInt(ram.getColumnIndex("price"));
            now = ram.getInt(ram.getColumnIndex("price"));
            total +=now;

            newprice=String.valueOf(now);
            ram=sq.rawQuery("select * from ram where price <" + newprice + " order by price DESC",null);
            ram.moveToFirst();
            if (!ram.isAfterLast()) {
                txtstr11 =ram.getString(ram.getColumnIndex("ram_name"));
                x = ram.getInt(ram.getColumnIndex("price"))-now;
                ra1 = x;
                txtstr11 += "[" + String.valueOf(x) + "BDT]";


                ram=sq.rawQuery("select * from ram where price >" + newprice + " order by price ASC",null);
                ram.moveToFirst();
                if (!ram.isAfterLast()) {
                    txtstr12 = ram.getString(ram.getColumnIndex("ram_name"));
                    x=ram.getInt(ram.getColumnIndex("price"))-now;
                    ra2=x;
                    txtstr12 += "[+" + String.valueOf(x) + "BDT]";

                }
            }
        }




//powersupply
        x = (budget * 10) / 100;
        if(x<powlow)
            x=powlow;
        newprice = String.valueOf(x);
        Cursor powersupply= sq.rawQuery("select * from powersupply where price<=" + newprice + " order by price DESC", null);
        powersupply.moveToFirst();
        if(!powersupply.isAfterLast()) {
            txtstr5 = "POWERSUPPLY\n" + powersupply.getString(powersupply.getColumnIndex("powersupply_name")) + "\n" + "spec: " + powersupply.getString(powersupply.getColumnIndex("spec")) + "\n" + "watt: " + powersupply.getInt(powersupply.getColumnIndex("watt")) + "W\n" + "price: " + powersupply.getInt(powersupply.getColumnIndex("price"));
            now = powersupply.getInt(powersupply.getColumnIndex("price"));
            total +=now;

            newprice=String.valueOf(now);
            powersupply=sq.rawQuery("select * from powersupply where price <" + newprice + " order by price DESC",null);
            powersupply.moveToFirst();
            if (!powersupply.isAfterLast()) {
                txtstr13 =powersupply.getString(powersupply.getColumnIndex("powersupply_name"));
                x = powersupply.getInt(powersupply.getColumnIndex("price"))-now;
                pw1 = x;
                txtstr13 += "[" + String.valueOf(x) + "BDT]";


                powersupply=sq.rawQuery("select * from powersupply where price >" + newprice + " order by price ASC",null);
                powersupply.moveToFirst();
                if (!powersupply.isAfterLast()) {
                    txtstr14 = powersupply.getString(powersupply.getColumnIndex("powersupply_name"));
                    x=powersupply.getInt(powersupply.getColumnIndex("price"))-now;
                    pw2=x;
                    txtstr14 += "[+" + String.valueOf(x) + "BDT]";

                }
            }

        }




//casing
        x = (budget * 10) / 100;
        if(x<caslow)
            x=caslow;
        newprice = String.valueOf(x);
        Cursor casing= sq.rawQuery("select * from casing where price<=" + newprice + " order by price DESC", null);
        casing.moveToFirst();
        if(!casing.isAfterLast()) {
            txtstr6 = "CASING\n" + casing.getString(casing.getColumnIndex("casing_name")) + "\n" + "price: " + casing.getInt(casing.getColumnIndex("price"));
            now = casing.getInt(casing.getColumnIndex("price"));
            total +=now;

            newprice=String.valueOf(now);
            casing=sq.rawQuery("select * from casing where price <" + newprice + " order by price DESC",null);
            casing.moveToFirst();
            if (!casing.isAfterLast()) {
                txtstr15 =casing.getString(casing.getColumnIndex("casing_name"));
                x = casing.getInt(casing.getColumnIndex("price"))-now;
                ca1= x;
                txtstr15 += "[" + String.valueOf(x) + "BDT]";


                casing=sq.rawQuery("select * from casing where price >" + newprice + " order by price ASC",null);
                casing.moveToFirst();
                if (!casing.isAfterLast()) {
                    txtstr16 = casing.getString(casing.getColumnIndex("casing_name"));
                    x=casing.getInt(casing.getColumnIndex("price"))-now;
                    ca2=x;
                    txtstr16 += "[+" + String.valueOf(x) + "BDT]";

                }
            }
        }





//HDD
        x = (budget * 10) / 100;
        if(x<hddlow)
            x=hddlow;
        newprice = String.valueOf(x);
        Cursor harddisk= sq.rawQuery("select * from harddisk where price<=" + newprice + " order by price DESC", null);
        harddisk.moveToFirst();
        if(!harddisk.isAfterLast()) {
            txtstr7 = "HARDDISK\n" + harddisk.getString(harddisk.getColumnIndex("hard_name")) + "\n" + "price: " + harddisk.getInt(harddisk.getColumnIndex("price"));
            now = harddisk.getInt(harddisk.getColumnIndex("price"));
            total +=now;

            newprice=String.valueOf(now);
            harddisk=sq.rawQuery("select * from harddisk where price <" + newprice + " order by price DESC",null);
            harddisk.moveToFirst();
            if (!harddisk.isAfterLast()) {
                txtstr17 =harddisk.getString(harddisk.getColumnIndex("hard_name"));
                x = harddisk.getInt(harddisk.getColumnIndex("price"))-now;
                hd1 = x;
                txtstr17 += "[" + String.valueOf(x) + "BDT]";


                harddisk=sq.rawQuery("select * from harddisk where price >" + newprice + " order by price ASC",null);
                harddisk.moveToFirst();
                if (!harddisk.isAfterLast()) {
                    txtstr18 = harddisk.getString(harddisk.getColumnIndex("hard_name"));
                    x=harddisk.getInt(harddisk.getColumnIndex("price"))-now;
                    hd2=x;
                    txtstr18 += "[+" + String.valueOf(x) + "BDT]";

                }
            }
        }



//processor
        if(type.equals("Professional PC"))
        {
            if((budget-total)<(i5_price+gplow))
                x = budget-total;
            else
                x =((budget - total)*67/100);
        }
        else
            x = ((budget - total)*43/100);


        newprice = String.valueOf(x);
        Cursor processor= sq.rawQuery("select * from processor where price<=" + newprice + " order by price DESC", null);
        processor.moveToFirst();
        if(!processor.isAfterLast()) {
            txtstr1 = "PROCESSOR\n" + processor.getString(processor.getColumnIndex("processor_name")) + "\n" + "core_count: " + processor.getInt(processor.getColumnIndex("core_count")) + "\n" + "clock_speed: " + processor.getFloat(processor.getColumnIndex("clock_speed")) + "GHz\n" + "gen: " + processor.getString(processor.getColumnIndex("gen")) + "\n" + "cache: " + processor.getFloat(processor.getColumnIndex("cache")) + "MB\n" + "socket: " + processor.getString(processor.getColumnIndex("socket")) + "\n" + "price: " + processor.getInt(processor.getColumnIndex("price"));
            now = processor.getInt(processor.getColumnIndex("price"));
            total +=now;

            newprice=String.valueOf(now);
            processor=sq.rawQuery("select * from processor where price <" + newprice + " order by price DESC",null);
            processor.moveToFirst();
            if (!processor.isAfterLast()) {
                txtstr19 =processor.getString(processor.getColumnIndex("processor_name"));
                x = processor.getInt(processor.getColumnIndex("price"))-now;
                pr1 = x;
                txtstr19 += "[" + String.valueOf(x) + "BDT]";


                processor=sq.rawQuery("select * from processor where price >" + newprice + " order by price ASC",null);
                processor.moveToFirst();
                if (!processor.isAfterLast()) {
                    txtstr20 = processor.getString(processor.getColumnIndex("processor_name"));
                    x=processor.getInt(processor.getColumnIndex("price"))-now;
                    pr2=x;
                    txtstr20 += "[+" + String.valueOf(x) + "BDT]";

                }
            }
        }





//gpu
        if(type.equals("Professional PC"))
        {
           if((budget-total)<gplow)
           {
               x=0;
               bad=1;
           }
           else
               x=(budget-total);
        }
        else
            x=(budget-total);


        newprice = String.valueOf(x);
        Cursor gpu= sq.rawQuery("select * from gpu where price<=" + newprice + " order by price DESC", null);
        gpu.moveToFirst();
        if(!gpu.isAfterLast()) {
            txtstr2 = "GPU\n" + gpu.getString(gpu.getColumnIndex("gpu_name")) + "\n" + "memory: " + gpu.getInt(gpu.getColumnIndex("memory")) + " GB\n" + "memory_type: " + gpu.getString(gpu.getColumnIndex("memory_type")) + "\n" + "clock_speed: " + gpu.getFloat(gpu.getColumnIndex("clock_speed")) + "\n"+"memory_clock: " + gpu.getFloat(gpu.getColumnIndex("memory_clock")) + "\n" + "price: " + gpu.getInt(gpu.getColumnIndex("price"));
            now = gpu.getInt(gpu.getColumnIndex("price"));
            total +=now;

            newprice=String.valueOf(now);
            gpu=sq.rawQuery("select * from gpu where price <" + newprice + " order by price DESC",null);
            gpu.moveToFirst();
            if (!gpu.isAfterLast()) {
                txtstr21 =gpu.getString(gpu.getColumnIndex("gpu_name"));
                x = gpu.getInt(gpu.getColumnIndex("price"))-now;
                gp1 = x;
                txtstr21 += "[" + String.valueOf(x) + "BDT]";


                gpu=sq.rawQuery("select * from gpu where price >" + newprice + " order by price ASC",null);
                gpu.moveToFirst();
                if (!gpu.isAfterLast()) {
                    txtstr22 = gpu.getString(gpu.getColumnIndex("gpu_name"));
                    x=gpu.getInt(gpu.getColumnIndex("price"))-now;
                    gp2=x;
                    txtstr22 += "[+" + String.valueOf(x) + "BDT]";

                }
            }
        }





        sq.close();
        txtstr8="Total Price: "+String.valueOf(total)+" BDT";

    }
}
