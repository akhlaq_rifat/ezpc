package com.example.rifatrahman.ezpc;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.example.rifatrahman.ezpc.Database.DBhelper;

import static com.example.rifatrahman.ezpc.Model.DbModel.DBname;
import static com.example.rifatrahman.ezpc.Model.DbModel.DBversion;


public class homepage extends Activity {

    EditText editText;
    Button button;
    TextView display,textView3,textView4;
    RadioGroup type_of_pc;
    RadioButton gaming;
    RadioButton professional;
    RadioButton radioButton;
    DBhelper d;
    Animation frombottom,fromtop,fadein,fromleft,fromright;

    public static String txtstr1="",txtstr2="",txtstr3="",txtstr4="",txtstr5="",txtstr6="",txtstr7="",txtstr8="",txtstr9="",txtstr10="",txtstr11="",txtstr12="",txtstr13="",txtstr14="",txtstr15="",txtstr16="",txtstr17="",txtstr18="",txtstr19="",txtstr20="",txtstr21="",txtstr22="";

    public static int mo1=0,mo2=0,ra1=0,ra2=0,pw1=0,pw2=0,ca1=0,ca2=0,hd1=0,hd2=0,gp1=0,gp2=0,pr1=0,pr2=0;


    public static String type="";
    public static int total=0,budget=0,least=0,x=0,bad=0;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homepage);

        type_of_pc=(RadioGroup) findViewById(R.id.type);
        editText = (EditText) findViewById(R.id.bud);
        button = (Button) findViewById(R.id.button);
        textView3= (TextView) findViewById(R.id.textView3);
        textView4= (TextView) findViewById(R.id.textView4);

        frombottom = AnimationUtils.loadAnimation(this,R.anim.frombottom);
        fromtop = AnimationUtils.loadAnimation(this,R.anim.fromtop);
        fromleft = AnimationUtils.loadAnimation(this,R.anim.fromleft);
        fromright = AnimationUtils.loadAnimation(this,R.anim.fromright);
        fadein = AnimationUtils.loadAnimation(this,R.anim.fadein);

        button.setAnimation(frombottom);
        textView3.setAnimation(fromleft);
        textView4.setAnimation(fromright);
        type_of_pc.setAnimation(fadein);
        editText.setAnimation(fadein);

        d= new DBhelper(this);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String bu=editText.getText().toString();

                if (bu.equals(""))
                    budget=0;
                else
                    budget=Integer.parseInt(bu);

                int selectedtype = type_of_pc.getCheckedRadioButtonId();
                radioButton = (RadioButton) findViewById(selectedtype);
                type = (String) radioButton.getText();

                d.least_price();

                if(d.checkvalidity()) {
                    d.getdata();
                    Intent intent = new Intent( homepage.this,showbuild.class);
                    startActivity(intent);
                }
                else {
                    Intent intent = new Intent( homepage.this,buildshow.class);
                    startActivity(intent);
                }

            }
        });

    }
}

