package com.example.rifatrahman.ezpc;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;

public class starting extends Activity {
    Animation fromleft,fromright,toleft,toright;
    Button button1, button3;
    Button button2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_starting);



        button1=(Button) findViewById(R.id.but1);
        button2=(Button) findViewById(R.id.but2);
        button3=(Button) findViewById(R.id.but3);

        fromleft = AnimationUtils.loadAnimation(this,R.anim.fromleft);
        fromright = AnimationUtils.loadAnimation(this,R.anim.fromright);
        toleft = AnimationUtils.loadAnimation(this,R.anim.toleft);
        toright = AnimationUtils.loadAnimation(this,R.anim.toright);

        button1.setAnimation(fromleft);
        button2.setAnimation(fromright);
        button3.setAnimation(fromleft);

        button1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                button1.startAnimation(toleft);
                button2.startAnimation(toright);
                button3.startAnimation(toleft);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent( starting.this,homepage.class);
                        startActivity(intent);
                    }
                }, 300);
            }
        });
        button2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                button1.startAnimation(toleft);
                button2.startAnimation(toright);
                button3.startAnimation(toleft);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent( starting.this,buildshow.class);
                        startActivity(intent);
                    }
                }, 300);

            }
        });
        button3.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                button1.startAnimation(toleft);
                button2.startAnimation(toright);
                button3.startAnimation(toleft);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent( starting.this,buildshow.class);
                        startActivity(intent);
                    }
                }, 300);
            }
        });

    }
}
